﻿<%@ Page Title="Quienes Somos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="RegistroAcademia.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <br />
    <h3>Conoce nuestra Academia</h3>
    <br />
    <div>
        <h4>En nuestra academia buscamos la excelencia</h4>
        <p>El objetivo de nuestros profesores es que los alumnos aprendan a aprender, estamos seguros que nadie mejor qu tú para conocer tus necesidades, y si no sabes algo
            ahí está nuestro equipo docente para indicarte el camino, pero recuerda que el camino lo tienes que andar tú, y serás tú quién consiga los resultados que buscas, 
            nostros solo te ayudaremos a levantarte cuando te caigas :)
        </p>
    </div>
    

</asp:Content>
