﻿<%@ Page Title="Contacto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="RegistroAcademia.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Donde estamos</h3>
    <address>
        Avd. de los Filósofos s/n<br />
        Leganés, 28917 Madrid<br />
        <abbr title="Phone">P:</abbr>
        +34 91 008 34 22
    </address>

    <address>
        <strong>Nuevos Alumnos:</strong>   <a href="mailto:Support@example.com">nuevos_alumnos@yosolosequenosenada.com</a><br />
        <strong>Atención al Alumno:</strong> <a href="mailto:Marketing@example.com">atencion_al_alumno@yosolosequenosenada.com</a>
    </address>
</asp:Content>
