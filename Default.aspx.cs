﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RegistroAcademia
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool mostrar_registro = false;
            bool mostrar_error = false;

            MostrarRegistro();

            MostrarError();

            void MostrarRegistro()
            {
                if (mostrar_registro == false)
                {
                    registrado.Visible = false;
                }
            }

            void MostrarError() 
            {
                if (mostrar_error == false) 
                {
                    no_existe.Visible = false;
                }
            }
        }

        protected void loginButton(object sender, EventArgs e) 
        {
            string _usuario = Convert.ToString(usuario.Text);
            string[] profesores = { "Enrique", "Celia", "Kakashi", "kaiho Shin", "Jiraiya", "Goku", "Naruto", "Piccolo" };
            

            var registro = Array.Exists(profesores, x => x == _usuario);

            if (registro == true)
            {

                registrado.Visible = true;
                registrado.Text = Convert.ToString("Usuario: " + _usuario + " registrado" + DateTime.Now.ToString());
            }
            else 
            {
                no_existe.Visible = true;
                no_existe.Text = Convert.ToString("Usuario: " + _usuario + " !NO EXISTE¡");
            }
        }

      
    }
}